<?php

namespace App\Http\Controllers;

use Faker\Factory AS Faker;
use Illuminate\Http\Request;

class AngularController extends Controller{
	public function randomPersons(){
		$faker = Faker::create();

		for($i = 0; $i < 20; $i++){
			$data[$i]['pipol'] = $faker->firstname .  " " . $faker->lastname;
			$data[$i]['img'] = "https://api.adorable.io/avatars/50/" . strtolower($faker->firstname) . "@" . strtolower($faker->lastname);
		}

		return json_encode($data);
	}
}