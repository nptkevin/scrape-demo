<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">

	<title>{{ config('app.name') }}</title>

	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-animate.js"></script>
	<script type="text/javascript">
		var app = angular.module('scrape-angular', [], function($interpolateProvider){
			$interpolateProvider.startSymbol('{~');
			$interpolateProvider.endSymbol('~}');
		});
	</script>

	@yield('headerstyles')

</head>
<body ng-app="scrape-angular">
	<section class="" id="main-navbar">
		@include('layouts.navbar')
	</section>

	<section class="hidden" id="main-carousel">
		@include('layouts.carousel')
	</section>

	<section class="" id="angular-js">
		@yield('content')
	</section>

	<section class="hidden" id="buttons">
		@include('layouts.buttons')
	</section>

	<section class="hidden" id="typography">
		@include('layouts.typography')
	</section>

	@include('layouts.footer')

</body>
</html>