<div class="container bs-section">
	<div class="row">
		<div class="col-lg-12">
			<div class="page-header">
				<h1 id="buttons">Buttons</h1>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<p class="">
				<button type="button" class="btn btn-primary">Primary</button>
				<button type="button" class="btn btn-secondary">Secondary</button>
				<button type="button" class="btn btn-success">Success</button>
				<button type="button" class="btn btn-info">Info</button>
				<button type="button" class="btn btn-warning">Warning</button>
				<button type="button" class="btn btn-danger">Danger</button>
				<button type="button" class="btn btn-link">Link</button>
			</p>

			<p class="">
				<button type="button" class="btn btn-primary disabled">Primary</button>
				<button type="button" class="btn btn-secondary disabled">Secondary</button>
				<button type="button" class="btn btn-success disabled">Success</button>
				<button type="button" class="btn btn-info disabled">Info</button>
				<button type="button" class="btn btn-warning disabled">Warning</button>
				<button type="button" class="btn btn-danger disabled">Danger</button>
				<button type="button" class="btn btn-link disabled">Link</button>
			</p>

			<p class="">
				<button type="button" class="btn btn-outline-primary">Primary</button>
				<button type="button" class="btn btn-outline-secondary">Secondary</button>
				<button type="button" class="btn btn-outline-success">Success</button>
				<button type="button" class="btn btn-outline-info">Info</button>
				<button type="button" class="btn btn-outline-warning">Warning</button>
				<button type="button" class="btn btn-outline-danger">Danger</button>
			</p>
		</div>
	</div>
</div>