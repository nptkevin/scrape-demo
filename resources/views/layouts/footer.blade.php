<div class="container bs-section">
	<footer id="footer">
		<div class="row">
			<div class="col-lg-12">
				<ul class="list-unstyled">
					<li class="float-lg-right"><a href="#top">Back to top</a></li>
					<li><a href="http://blog.bootswatch.com" onclick="pageTracker._link(this.href); return false;">Blog</a></li>
					<li><a href="https://feeds.feedburner.com/bootswatch">RSS</a></li>
					<li><a href="https://twitter.com/bootswatch">Twitter</a></li>
					<li><a href="https://github.com/thomaspark/bootswatch/">GitHub</a></li>
					<li><a href="../help/#api">API</a></li>
					<li><a href="../help/#donate">Donate</a></li>
				</ul>

				<p>Made by <a href="http://thomaspark.co">Thomas Park</a>.</p>
				<p>Code released under the <a href="https://github.com/thomaspark/bootswatch/blob/master/LICENSE">MIT License</a>.</p>
				<p>Based on <a href="https://getbootstrap.com" rel="nofollow">Bootstrap</a>. Icons from <a href="http://fontawesome.io/" rel="nofollow">Font Awesome</a>. Web fonts from <a href="https://fonts.google.com/" rel="nofollow">Google</a>.</p>
			</div>
		</div>
	</footer>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

{{-- AngularJS Scripts --}}
@yield('angularjs')
@yield('footerscripts')