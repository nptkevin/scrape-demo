@extends('welcome')

@section('headerstyles')
@endsection

@section('content')
	<div class="container bs-section" ng-controller="randomPersons">
		<a href="#" class="btn btn-primary" ng-click="generatePipol()">Generate</a>

		<br /><br />

		<div class="list-group">
			<a class="list-group-item" ng-repeat="x in myData">
				<img src="{~ x.img ~}" width="50"> &nbsp; {~ x.pipol ~}
			</a>
		</div>
	</div>
@endsection

@section('angularjs')
	<script type="text/javascript">
		app.controller('randomPersons', function($scope, $http){
			$scope.generatePipol = function(){
				$http.post("{{ route('pipols') }}").then(function(response){
					$scope.myData = response.data;
				});
			};
		});
	</script>
@endsection

@section('footerscripts')
@endsection